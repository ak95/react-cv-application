import React from 'react';
import ReactDOM from 'react-dom/client';
import Identification from './components/Identification.jsx';
import Education from './components/Education.jsx';
import './styles/main.scss';
import Employment from './components/Employment.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <div className="d-flex flex-column bg-dark text-white h-100 overflow-auto">
      <header>
        <h3 className="py-2 mb-0 border-bottom border-white">CV</h3>
      </header>
      <div className="d-flex h-100 overflow-auto">
        <div className="d-flex flex-column overflow-auto Left">
          <Identification />
          <Education />
        </div>
        <div className="d-flex flex-column flex-1-0-auto overflow-auto Right">
          <Employment />
        </div>
      </div>
    </div>
  </React.StrictMode>,
)
