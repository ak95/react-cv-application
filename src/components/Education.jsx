import { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import EducationSlot from './EducationSlot.jsx';
import { v4 as uuidv4 } from 'uuid';

export default function Education () {
    const [slots, setSlots] = useState([]);

    const newButton = () => { // makes the new button
        function clicked () {
            setSlots([...slots, (<EducationSlot key={uuidv4()}/>)]);
        }
        return (
            <button type="button" className="btn btn-outline-light mt-4" onClick={clicked}>
                <FontAwesomeIcon icon={faPlus} /> Add education experience
            </button>
        );
    }
    
    return (
        <div className="border-end border-white flex-1-0-auto Education">
            <div>
                {slots}
            </div>
            {newButton()}
        </div>
    )
}