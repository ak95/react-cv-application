import { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import EmploymentSlot from './EmploymentSlot.jsx';
import { v4 as uuidv4 } from 'uuid';

export default function Employment () {
    const [slots, setSlots] = useState([]);

    const newButton = () => { // makes the new button
        function clicked () {
            setSlots([...slots, (<EmploymentSlot key={uuidv4()}/>)]);
        }
        return (
            <button type="button" className="btn btn-outline-light mt-4" onClick={clicked}>
                <FontAwesomeIcon icon={faPlus} /> Add employment experience
            </button>
        );
    }
    
    return (
        <div className="flex-1-0-auto Employment">
            {slots}
            {newButton()}
        </div>
    )
}