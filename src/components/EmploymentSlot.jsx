import { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faPencil } from "@fortawesome/free-solid-svg-icons";

export default function EmploymentSlot () { // one education slot containing institution, degree and discipline fields, triggered by click of new button
    const [company, setCompany] = useState('');
    const [position, setPosition] = useState('');
    const [fromYear, setFromYear] = useState('');
    const [toYear, setToYear] = useState('');
    const [responsibilities, setResponsibilities] = useState('');
    const currentYear = new Date().getFullYear();

    const handleChange = event => {
        if (event.target.id === 'Company') setCompany(event.target.value);
        if (event.target.id === 'Position') setPosition(event.target.value);
        if (event.target.id === 'fromYear') setFromYear(event.target.value);
        if (event.target.id === 'toYear') setToYear(event.target.value);
        if (event.target.id === 'Responsibilities') setResponsibilities(event.target.value);
    };
    
    const makeSelect = type => { // makes select fields
        const [active, setActive] = useState(true);
        const selectOptions = [];
        let stateVar, internalText, flavorText;

        if (type === `fromYear`) {
            stateVar = fromYear;
            internalText = type;
            flavorText = '';
            for (let i = 0; i <= 100; i++) {
                const loopYear = currentYear - 100 + i; // loops from 1923 to 2023, or whatever your current year is
                selectOptions.push(<option value={loopYear}>{loopYear}</option>);
            }
        }

        if (type === `toYear`) {
            stateVar = toYear;
            internalText = type;
            flavorText = '';
            for (let i = 0; i <= 100; i++) {
                const loopYear = currentYear - 100 + i; // loops from 1923 to 2023, or whatever your current year is
                selectOptions.push(<option value={loopYear}>{loopYear}</option>);
            }
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }

        const submitButton = () => {
            function clicked () {
                if (active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex align-items-center gap-2">
                <div>
                    <label htmlFor={internalText}>{flavorText}</label>
                </div>
                <select 
                    id={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-select bg-dark text-white"
                    disabled={!active}
                    >
                    {selectOptions}
                </select>
                {editButton()}
                {submitButton()}
            </div>
        )

    }

    const makeInput = type => { // makes text input fields
        const [active, setActive] = useState(true);
        let stateVar, internalText, flavorText;

        if (type === `Company`) {
            stateVar = company;
            internalText = flavorText = type;
        }
        if (type === `Position`) {
            stateVar = position;
            internalText = type;
            flavorText = `Role`;
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }
    
        const submitButton = () => {
            function clicked (event) {
                if (active) {
                    if (event.target.previousSibling.previousSibling.id === `Company` && company === '') return alert(`Please enter name of company`);
                    if (event.target.previousSibling.previousSibling.id === `Position` && position === '') return alert(`Please enter position held at ${company}`);
                    setActive(!active);
                }
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex gap-2 align-items-center">
                <div>
                    <label htmlFor={internalText}>{flavorText}:</label>
                </div>
                <input 
                    type="text"
                    id={internalText}
                    name={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-control bg-dark text-white"
                    disabled={!active}
                />
                {editButton()}
                {submitButton()}
            </div>
        )
    };

    const makeTextArea = type => { // makes textarea fields
        const [active, setActive] = useState(true);
        let stateVar, internalText, flavorText;

        if (type === `Responsibilities`) {
            stateVar = responsibilities;
            internalText = flavorText = type;
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }
    
        const submitButton = () => {
            function clicked (event) {
                if (active) {
                    if (event.target.previousSibling.previousSibling.id === `Responsibilities` && responsibilities === '') return alert(`Please enter job responsibilities`);
                    setActive(!active);
                }
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex gap-3 align-items-center">
                <label htmlFor={internalText}>{flavorText}:</label>
                <textarea 
                    id={internalText}
                    name={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-control bg-dark text-white"
                    disabled={!active}
                    rows={10}
                />
                <div className="d-flex flex-column gap-4 align-items-center">
                    {editButton()}
                    {submitButton()}
                </div>
            </div>
        )
    }

    return (
        <>
            <div className="d-flex p-4 gap-3 justify-content-start align-items-center">
                {makeInput('Company')}
                {makeInput('Position')}
                {makeSelect('fromYear')} to {makeSelect('toYear')}
            </div>
            <div className="d-flex flex-column p-4 gap-3 border-bottom border-white">
                {makeTextArea('Responsibilities')}
            </div>
        </>
    );
}