import { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faPencil } from "@fortawesome/free-solid-svg-icons";

export default function Identification () {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const handleChange = event => {
        if (event.target.id === 'Name') setName(event.target.value);
        if (event.target.id === 'Email') setEmail(event.target.value);
        if (event.target.id === 'Phone') { // perform validation to ensure phone number is a string of 10 digits (USA standard)
            const filteredValue = event.target.value.match(/(\d+)/gi);
            if (filteredValue === null) setPhone('');
            else if (filteredValue[0].length <= 10) setPhone(filteredValue); //filteredValue[0] because String.match() returns an Array containing a String at index 0
        }
    };

    const makeInput = type => {
        const [active, setActive] = useState(true);
        let stateVar, internalText, flavorText;

        if (type === `Name`) {
            stateVar = name;
            internalText = flavorText = type;
        }
        if (type === `Email`) {
            stateVar = email;
            internalText = type;
            flavorText = `Email Address`;
        }
        if (type === `Phone`) {
            stateVar = phone;
            internalText = type;
            flavorText = `Phone Number`;
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }
    
        const submitButton = () => {
            function clicked (event) {
                if (active) {
                    if (event.target.previousSibling.previousSibling.id === `Email`) { // check for email input field and perform validation
                        const atIndex = email.indexOf(`@`);
                        if (atIndex < 0) return alert(`E-mail is invalid`); // if no @ found, email is invalid
                        else {
                            const dotIndex = email.indexOf(`.`, atIndex + 1);
                            if (dotIndex < 0) return alert(`E-mail is invalid`); // if no . found after the @, email is invalid
                        }
                    }
                    if (event.target.previousSibling.previousSibling.id === `Name` && name === '') return alert(`Please enter name`);
                    if (event.target.previousSibling.previousSibling.id === `Phone` && phone === '') return alert(`Please enter phone number`);
                    setActive(!active);
                }
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex gap-3 align-items-center">
                <div className="flex-15">
                    <label htmlFor={internalText}>{flavorText}:</label>
                </div>
                <input 
                    type="text"
                    id={internalText}
                    name={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-control bg-dark text-white"
                    disabled={!active}
                />
                {editButton()}
                {submitButton()}
            </div>
        )
    };

    return (
        <div className="d-flex flex-column p-4 gap-3 justify-content-start border-bottom border-end border-white Identification">
            {makeInput('Name')}
            {makeInput('Email')}
            {makeInput('Phone')}
        </div>
    );
}