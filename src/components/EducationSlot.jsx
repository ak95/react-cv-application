import { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faPencil } from "@fortawesome/free-solid-svg-icons";

export default function EducationSlot () { // one education slot containing institution, level and discipline fields, triggered by click of new button
    const [institution, setInstitution] = useState('');
    const [level, setLevel] = useState('');
    const [discipline, setDiscipline] = useState('');
    const [graduationYear, setGraduationYear] = useState('');
    const currentYear = new Date().getFullYear();

    const handleChange = event => {
        if (event.target.id === 'Year') setGraduationYear(event.target.value);
        if (event.target.id === 'Level') setLevel(event.target.value);
        if (event.target.id === 'Institution') setInstitution(event.target.value);
        if (event.target.id === 'Discipline') setDiscipline(event.target.value);
    }

    const makeSelect = type => {
        const [active, setActive] = useState(true);
        const selectOptions = [];
        let stateVar, internalText, flavorText;

        if (type === `Year`) {
            stateVar = graduationYear;
            internalText = flavorText = type;
            for (let i = 0; i <= 100; i++) {
                const loopYear = currentYear - 100 + i; // loops from 1923 to 2023, or whatever your current year is
                selectOptions.push(<option value={loopYear}>{loopYear}</option>);
            }
        }
        if (type === `Level`) {
            stateVar = level;
            internalText = flavorText = type;
            const levelsText = [`High School`, `Associate's Degree`, `Bachelor's Degree`, `Master's Degree`, `Doctorate Degree`, `Other`];
            for (let i = 0; i < levelsText.length; i++) {
                selectOptions.push(<option value={levelsText[i]}>{levelsText[i]}</option>);
            }
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }

        const submitButton = () => {
            function clicked () {
                if (active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex gap-3 align-items-center">
                <div className="flex-15">
                    <label htmlFor={internalText}>{flavorText}:</label>
                </div>
                <select 
                    id={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-select bg-dark text-white"
                    disabled={!active}
                    >
                    {selectOptions}
                </select>
                {editButton()}
                {submitButton()}
            </div>
        )

    }

    const makeInput = type => { // makes text input fields
        const [active, setActive] = useState(true);
        let stateVar, internalText, flavorText;

        if (type === `Institution`) {
            stateVar = institution;
            internalText = flavorText = type;
        }
        if (type === `Discipline`) {
            stateVar = discipline;
            internalText = flavorText = type;
        }

        const editButton = () => {
            function clicked () {
                if (!active) setActive(!active);
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faPencil} />
                </button>
            );
        }
    
        const submitButton = () => {
            function clicked (event) {
                if (active) {
                    if (event.target.previousSibling.previousSibling.id === `Institution` && institution === '') return alert(`Please enter name of institution`);
                    if (event.target.previousSibling.previousSibling.id === `Discipline` && discipline === '') return alert(`Please enter educational discipline`);
                    setActive(!active);
                }
            }
            return (
                <button type="button" className="btn btn-outline-light" onClick={clicked}>
                    <FontAwesomeIcon icon={faFloppyDisk} />
                </button>
            );
        }

        return (
            <div className="d-flex gap-3 align-items-center">
                <div className="flex-15">
                    <label htmlFor={internalText}>{flavorText}:</label>
                </div>
                <input 
                    type="text"
                    id={internalText}
                    name={internalText}
                    onChange={handleChange}
                    value={stateVar}
                    className="form-control bg-dark text-white"
                    disabled={!active}
                />
                {editButton()}
                {submitButton()}
            </div>
        )
    };

    return (
        <div className="d-flex flex-column p-4 gap-3 justify-content-start border-bottom border-white">
            {makeInput('Institution')}
            {makeSelect('Level')}
            {makeInput('Discipline')}
            {makeSelect('Year')}
        </div>
    );
}